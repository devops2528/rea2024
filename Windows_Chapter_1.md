
# Chapter 1. Planet Express
## Устанавливаем AD CS role:
Install-WindowsFeature AD-Certificate -IncludeManagementTools 

## Задаем имя СА, Устанавливаем СА с сертификатом сроком жизни 4 года и указанным выше именем
$CA_Name = "OmicronPersei8"
Install-AdcsCertificationAuthority -CACommonName $CA_Name -CAType EnterpriseRootCa -HashAlgorithmName SHA256 -KeyLength 2048 -ValidityPeriod Years -ValidityPeriodUnits 4 -force

## Добавляем web-enrollment
Install-WindowsFeature ADCS-Web-Enrollment -IncludeManagementTools

## Перезагружаем сервер
shutdown -r -f -t 0

## Включаем корзину
Enable-ADOptionalFeature -Identity "CN=Recycle Bin Feature,CN=Optional Features,CN=Directory Service,N=Windows NT,CN=Services,CN=Configuration,DC=planet,DC=express" -Scope ForestOrConfigurationSet -Target "planet.express"

## Создаем GPO для веб страницы по умолчанию
New-GPO -Name "default_web_pages" 

## Линкуем GPO в домен
New-GPLink -Name "default_web_pages" -Target "DC=planet,DC=express"

## Устанавливаем параметры веб страницы по умолчанию
Set-GPRegistryValue -Name "default_web_pages" -Key "HKCU\Software\Policies\Microsoft\Edge\RestoreOnStartupURLs" -ValueName 1 -Type String -Value "https://che.bur.net"
Set-GPRegistryValue -Name "default_web_pages" -Key "HKCU\Software\Policies\Microsoft\Edge\" -ValueName "RestoreOnStartup" -Type DWORD -Value 4

## Создаем папку:
New-Item "c:\BotPlanet" -ItemType Directory
## Расшариваем папку
New-SmbShare -Name "BotPlanet" -Path "c:\BotPlanet" -FullAccess "planet\robots"
## Устанавливаем FSRM
## Default domain controllers Policy:
computer configuration -> policies -> administrative template -> system -> KDC -> Right-click “KDC support for claims, compound authentication, and Kerberos armoring” and enable it.
## Active Directory Administrative Center Открываем раздел Dynamic Access Control и переходим к подразделу Claim Type.  Кликаем правой клавишей мыши в поле и в открывшемся меню выбираем пункт New – Claim Type
name: Tittle_type
Source attrivute: title (что и есть job tittle в схеме AD)
Suggested Values: "Delivery boy"
## Открываем раздел Dynamic Access Control и переходим к подразделу Resource Properties.  Кликаем правой клавишей мыши в поле и в открывшемся меню выбираем пункт New – Resource Properties
name: Tittle_resource
Suggested Values: "Delivery boy"
## Переходим в раздел Central Access Rule, кликаем правой клавишей в пустом поле и выбираем пункт New – Central Access Rule
name: Tittle_car
Target Resources -> Edit -> Add a condition -> Resource - Tittle_resource - Equals - Value - "Delivery Boy"
Use following permissions as current permissions
Edit -> Add Authenticated Users -> Full Control -> Add Condition:
User - title - equals - Value - Delivery boy
Or 
User - group - member of each - Value - planet\robots
## Переходим в раздел Central Access Policies, правый клик мышкой — New — Central Access Policy
name: Tittle_cap
member: tittle_car
## Создаем новую GPO: DAC и линкуем на домен
Переходим в раздел Computer Configuration\Policies\Windows Settings\Security Settings\File System\Central Access Policy. Кликаем правой клавишей и выбираем «Manage Central Access Policies».
Добавлякм Title_rule
## Обновляем классификацию файлов
Update-FSRMClassificationPropertyDefinition
Свойства папки - classification, name - Tittle_resource - "Delivery boy"
Gpupdate /force

Install-WindowsFeature –Name FS-Resource-Manager –IncludeManagementTools
## Включаем ADR settings и устанавливаем сообщение:
Set-fsrmadrsettings -displaymessage "I'm Going To Build My Own Theme Park With Blackjack and
Hookers" -event accessdenied -enabled
## Политикой включаем ADR на клиенте
New-GPO -Name "ADR" 
New-GPLink -Name "ADR" -Target "DC=planet,DC=express"
Set-GPRegistryValue -Name "ADR" -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\Explorer\" -ValueName "EnableShellExecuteFileStreamCheck" -Type DWORD -Value 1

## Включаем временное членство в группе на уровне AD
Enable-ADOptionalFeature 'Privileged Access Management Feature' -Scope ForestOrConfigurationSet -Target “planet.express"
## добавляем пользователя
$ttl = new-timespan -days 5
Add-ADGroupMember -Identity "aliens" -Members leela -MemberTimeToLive $ttl

## Разрешаем интерактивный вход на домен контроллер
Default Domain Controller Policy: Computer Configuration> Policies > Windows Settings > Security Settings > Local Policies > user Rights Assignmment > Allow log on locally. добавляем пользователя bender


## Приветствие:
### Создаем файл c:\scripts\error.vbs со следующим содержимым
x=msgbox ("Im Going To Build My Own Theme Park With Blackjack and shlukshs",48,"error")
## Создаем задание в task manager
$taskTrigger = New-ScheduledTaskTrigger -AtLogon -User "planet\bender"

$taskAction = New-ScheduledTaskAction -Execute "C:\Windows\System32\wscript.exe" -Argument "C:\scripts\error.vbs"
Register-ScheduledTask 'logon' -Action $taskAction -Trigger $taskTrigger -User "planet\bender"

