# Chapter 2. The Lesser of Two Evils

## Создаем зону DNS bur.net
* Add-DnsServerPrimaryZone -Name "bur.net" -ReplicationScope "Forest" -PassThru 
## Создаем DNS записи che.bur.net, внимательно смотрим на TTL (в Гуях в оснасткек DNS включаем расширенные свойства)
* Add-DnsServerResourceRecordA -Name "che" -ZoneName "bur.net" -AllowUpdateAny -IPv4Address "11.11.11.2" -TimeToLive 00:00:01
* Add-DnsServerResourceRecordA -Name "che" -ZoneName "bur.net" -AllowUpdateAny -IPv4Address "11.11.11.3" -TimeToLive 00:00:01
* Add-DnsServerResourceRecordA -Name "che" -ZoneName "admin.bur.net" -AllowUpdateAny -IPv4Address "11.11.11.2" -TimeToLive 00:00:01
* Add-DnsServerResourceRecordA -Name "che" -ZoneName "admin.bur.net" -AllowUpdateAny -IPv4Address "11.11.11.3" -TimeToLive 00:00:01

## На сервер Earth устанавливаем DFS NameSpace
## На Сервера Bender1 и Bender2 устанавливаем IIS и DFS Replication. В IIS добавляем Application Development -> CGI, security -> windows Authentification, security -> URL Authorization, security -> IIS Client Certificate Mapping Authenification

## Создаем шаблон сертификата
* Certificate Authority -> Certificate Templates -> Manage -> Web Server -> Duplicate Template -> Display name -> WebServer_exported, Request Handling -> Allow private key to be exported, Security -> add Bender1(PC) -> full control
* Certificate templates -> new -> Certificate templates to issue -> WebServer_exported
* Certificate Authority -> свойства -> view certificate -> details -> copy to files -> Сохраняем в c:\temp\

## Создаем новую политику CA_cert
Computer configuration -> Policies -> Windows Settings -> Security Settings -> Public key Polices -> Trusted root certification authority -> add CA certificate

## Bender1
mmc -> ctrl + M -> certificates -> local pc -> personal -> request certificate -> Active directory enrollment policy -> WebServer_exported:
* CN: che.bur.net
* DNS: che.bur.net
* DNS: admin.che.bur.net

Правый мышь по новому сертификату -> export -> export with private key -> export all extended properties -> password -> 12345678 -> next
Копируем файлик (кладем в папку c:\portal) и устанавливаем сертификат на Bender2

## Создаем папку c:\portal на сервере bender1 и размещаем в ней файлы портала
## Идем на Earth DFS Managment -> DFSR replication -> сервера bender1 и bender2 -> папка c:\portal на обоиз серверах, bender1 - Primary 

## Настраиваем PHP на IIS (bender1 & bender2)

* Удаляем default site
* Создаем новый сайт che.bur.net - место расположение c:\portal\main
* https - имя che.bur.net, выбираем сертификат. Включаем SNI, отключаем TLS 1.3
* в папку c:\portl\php кладем файлы php

* IIS -> Handler Mappings -> Add module mapping:
* Request path: *.php 
* Module: FastCGIModule
* Executable: c:\portal\php\php-cgi.exe
* Name: PHP

* Default document -> add -> index.php

## Настраиваем SSO
### Создаем УЗ в домене IIS_service
### Включаем SPN для УЗ:
* setspn -S HTTP\che.bur.net planet\iis_service
### Даем полные права для УЗ planet\IIS_Service на папку c:\portal

### настройки серверов bender1 и bender2
* Настройки IIS, Application pools, che.bur.net -> Advanced Settings -> Identity: planet\iis_service
* Настройки IIS, Sites -> che.bur.net -> Configurator Editor -> system.Webserver/security/authentication/windowsAuthentication -> useAppPoolCredentials: true
* Настройки IIS, Sites -> che.bur.net -> Authentication -> Anonymous Authentication: Disabled, Windows Authentication: Enabled
* Настройки IIS, Sites -> che.bur.net -> Authorization Rules -> в существующее правило AlLow вписываем группу planet\portal

###  настройки GPO для килентов(не забываем установить ADM для MS Edge и Chrome):

* User configuration -> policies -> Administrative Templates -> Microsoft Edge -> HTTP authentification -> Configure list of allowed authentification servrs -> che.bur.net
* User configuration -> policies -> Administrative Templates -> Google -> HTTP authentification -> Authentification server allowlist -> che.bur.net

## настройка admin.che.bur.net

### Создаем шаблон сертификата
Certificate Authority -> Certificate Templates -> Manage -> User -> Duplicate ->
* General -> name -> user_enroll
* Security -> add -> p-admins -> Full Control 
* Subject Name -> снимаем чек боксы с "Include e-mail name in subject name" и "E-mail name" 

### Добавляем новый шаблон 
* Certificate templates -> new -> Certificate templates to issue -> user_enroll

### Включаем AutoEnroll сертификатов
* User configuration -> policies -> Windows Settings -> Security Settings -> Public key Policies -> Certificate Service Client - Auto-Enrollment Properties: Enabled, Ставим чекбоксы: Renew.., Update..

### Настройки IIS для admin.che.bur.net
* Удаляем default site
* Создаем новый сайт admin.che.bur.net - место расположение c:\portal\Admin
* https - имя admin.che.bur.net, выбираем сертификат. Включаем SNI, отключаем TLS 1.3
* Authentification -> везде должно стоять: Disable 
* SSL Settings -> Require SSL, Require

* system.webServer -> security -> authentification -> iisClientCertificateMappingAuthentication -> 
    * enabled: True
    * manyToOneCertificateMappingEnabled: True
    * manyToOneMappings -> add ->
        * name: cert
        * enabled: true
        * permissionMode: Allow
        * userNmae: portal\IIS_service
        * rules -> оставляем пустым

## Приотсутствии прав выдется страничка custom.htm
* Error Pages -> 401 -> Edit:
    * Respond with 302 redirect: https://che.bur.net/custom.htm
* Error Pages -> 403 -> Edit:
    * Respond with 302 redirect: https://che.bur.net/custom.htm
## Разрешаем скачивать файл +numbus+.rdp

* Mime type -> Add -> 
    * File name extension: .rdp
    * MIME type: applicatiom/rdp

* В IIS переходим в  admin.che.bur.net -> Files
* system.webServer -> security -> requestFiltering -> allowDoubleEscaping: True
* system.webServer -> directoryBrowse -> enabled:true



