# Chapter 3. The Beast with a Billion Backs
## Устанавливаем необходимые пакеты
* apt-cdrom add # Добавляем репозиторий с загрузочного диска
* apt install fly-admin-ad-sssd-client -y
## Вводим машинку в домен
* звезда -> панель управления -> "Сеть" -> "Настройка клиента SSSD Fly". Вводим название домена логин и пароль администратора
## Настраиваем fireFox, открываем  страницу about:config
* network.negotiate-auth.delegation-uris = che.bur.net
* network.negotiate-auth.trusted-uris = che.bur.net